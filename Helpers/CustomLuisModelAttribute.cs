﻿using Microsoft.Bot.Builder.Luis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LuisBot.Helpers
{
    [Serializable]
    public class CustomLuisModelAttribute : LuisModelAttribute
    {
        static string AppId = ConfigurationManager.AppSettings["LuisAppId"];
        static string LuisAPIKey = ConfigurationManager.AppSettings["LuisAPIKey"];
        static string LuisApiDomain = ConfigurationManager.AppSettings["LuisAPIHostName"];
        public CustomLuisModelAttribute(LuisApiVersion apiVersion = LuisApiVersion.V2, double threshold = 0) 
            : base(AppId, LuisAPIKey, apiVersion, LuisApiDomain, threshold)
        {
        }
    }
}