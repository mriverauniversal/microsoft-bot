﻿using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot.Helpers
{
    public static class Extensions
    {

        public static EntityRecommendation GetEntity(this LuisResult result, string entityType)
        {
            return result.Entities.FirstOrDefault(e => e.Type == entityType);
        }
    }
}