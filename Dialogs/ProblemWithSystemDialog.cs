﻿using LuisBot.Helpers;
using LuisBot.Interfaces;
using LuisBot.Services;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LuisBot.Dialogs
{
    [CustomLuisModel]
    [Serializable]
    public class ProblemWithSystemDialog : LuisDialog<object>
    {
        private string _systemName;

        [LuisIntent("ProblemWithSystem")]
        private async Task HandleInitialMessage(IDialogContext context, LuisResult luisResult)
        {
            var failingSystem = luisResult.GetEntity("System");
            _systemName = failingSystem.Entity;

            var service = new ExternalService();
            var applicationStates = await service.GetApplications();

            var systemIsReallyFailing = applicationStates.Any(s => s.name.ToLower() == _systemName
                                    && s.status == "INACTIVE");

            if (systemIsReallyFailing)
            {
                var inactiveApps = applicationStates.Where(a => a.status == "INACTIVE").ToList();
                await NotifyIncident(context, inactiveApps);
                return;
            }
            else {
                await context.PostAsync(failingSystem.Entity.ToUpper()+" esta bien.");
            }

            await context.PostAsync("Te esta presentando algun problema en especifico?");
        }

        [LuisIntent("SystemFroze")]
        public async Task SystemFroze(IDialogContext context, LuisResult luisResult)
        {
            context.Call(new SystemFrozeDialog(_systemName), ResumeAfterSystemFrozeDialog);
        }

        [LuisIntent("None")]
        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult luisResult)
        {
            var messageActivity = context.Activity.AsMessageActivity();
            context.Done(messageActivity);
        }
        private async Task NotifyIncident(IDialogContext context, List<MockSupportService.Models.Application> appList)
        {
            var appNames = appList.Select(s => s.name);
            var message = String.Join(", ", appNames);
            await context.PostAsync("Le informo que las siguientes aplicaciones estan Fuera de Servicio: "+ message + ". Tan pronto como esten disponibles le notificamos por correo.");
            context.Done(true);
        }
        private async Task ResumeAfterSystemFrozeDialog(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var value = await result;
                context.Done(value);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        protected override async Task MessageReceived(IDialogContext context, IAwaitable<IMessageActivity> item)
        {
           await base.MessageReceived(context, item);
        }
    }
}