using System;
using System.Linq;
using System.Threading.Tasks;
using LuisBot.Dialogs;
using LuisBot.Helpers;
using LuisBot.Services;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using MockSupportService.Models;

namespace Microsoft.Bot.Sample.LuisBot
{
    [Serializable]
    [CustomLuisModel]
    public class BaseLuisDialog : LuisDialog<object>
    {
        [LuisIntent("NotWork")]
        public async Task NotWorkIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Disculpe, creare una solicitud para dale una atencion mas personalizada.");
        }

        [LuisIntent("Wait")]
        public async Task WaitIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Ok, le espero");
        }

        [LuisIntent("NoResponse")]
        public async Task NoResponseIntent(IDialogContext context, LuisResult result)
        {

        }      

        [LuisIntent("Greeting")]
        public async Task GreetingIntent(IDialogContext context, LuisResult result)
        {
            var service = new ExternalService();
            var applicationStates = await service.GetApplications();
            var inactiveApps = applicationStates.Where(a => a.status == "INACTIVE").ToList();
            var appsMsj = "";
            if (inactiveApps.Count > 0) {
                var message = String.Join(",", inactiveApps.Select(p => p.name));
                appsMsj = "Le informo que las siguientes aplicaciones estan Fuera de Servicio: " + message + ". Tan pronto como esten disponibles le notificamos por correo.";
            }
            await context.PostAsync("Saludos. En que le puedo ayudar? "+ appsMsj);
        }

        [LuisIntent("Problem")]
        public async Task ProblemIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Dime que pasa, estoy aqui para ayudarte");
        }

        [LuisIntent("WhereAreYou")]
        public async Task WhereAreYouIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Estoy en el area de tecnologia.");
        }

        [LuisIntent("WhoAreYou")]
        public async Task WhoAreYouIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Soy parte del equipo de soporte.");
        }

        [LuisIntent("Thanks")]
        public async Task ThanksIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("No hay de que.");
        }

        [LuisIntent("Bye")]
        public async Task ByeIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Adios");
        }

        [LuisIntent("Help")]
        public async Task HelpIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Le puedo ayudar si se cae o se friza acsel, si tienes preguntas frecuentes, etc...");
        }

        [LuisIntent("ProblemWithSystem")]
        [LuisIntent("SystemFroze")]
        public async Task ProblemWithSystemIntent(IDialogContext context, LuisResult result)
        {
            var messageActivity = context.Activity.AsMessageActivity();
            await context.Forward(new ProblemWithSystemDialog(), AnotherAssitanceCallback, messageActivity);
        }

        [LuisIntent("InstallProgram")]
        public async Task InstallProgramIntent(IDialogContext context, LuisResult result)
        {
            var messageActivity = context.Activity.AsMessageActivity();
            await context.Forward(new InstallProgramDialog(), InstallProgramCallback, messageActivity);
        }

        [LuisIntent("HowAreYou")]
        public async Task HowAreYouIntent(IDialogContext context, LuisResult result)
        {
            var messageActivity = context.Activity.AsMessageActivity();
            await context.Forward(new HowAreYouDialog(), AfterHowAreYouDialog, messageActivity);
        }

        [LuisIntent("")]
        [LuisIntent("None")]
        public async Task FAQ(IDialogContext context, LuisResult result)
        {
            context.Call(new QnADialog(), AnotherAssitanceCallback);
        }
        private async Task InstallProgramCallback(IDialogContext context, IAwaitable<object> result)
        {
            //context.
        }

        private async Task AfterHowAreYouDialog(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            await this.MessageReceived(context, result);
        }

        private async Task AnotherAssitanceCallback(IDialogContext context, IAwaitable<object> result)
        {
            //PromptDialog.Confirm(context, ConfirmationCallback, "�Hay otra cosa en la que te pueda ayudar?");

        }
        private async Task ConfirmationCallback(IDialogContext context, IAwaitable<bool> result)
        {
            var repeatDialog = await result;
            if (repeatDialog)
            {
                await context.PostAsync("�En qu� te puedo ayudar?");
                context.Wait(MessageReceived);
            }
            else
            {
                await context.PostAsync("Fue un placer asistirle, que pase un buen d�a");
                var message = context.Activity.AsMessageActivity();
                context.Done(message);

            }
        }
    }
}