﻿using ApiMockService.Models;
using LuisBot.FormFlow;
using LuisBot.Helpers;
using LuisBot.Interfaces;
using LuisBot.Services;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Threading.Tasks;

namespace LuisBot.Dialogs
{
    [CustomLuisModel]
    [Serializable]
    public class InstallProgramDialog : LuisDialog<object>
    {
        private IExternalService _externalService;
        private string Program { get; set; }

        public InstallProgramDialog()
        {
            _externalService = new ExternalService();
        }

        [LuisIntent("InstallProgram")]
        public async Task InstallProgramIntent(IDialogContext context, LuisResult result)
        {
            if (LuisResultHasProgramEntity(result))
            {
                InitUserCodFlow(result, context);
            }
            else
            {
                await context.PostAsync("Cual es el programa que necesitas instalar?");
            }
        }

        [LuisIntent("None")]
        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            if (LuisResultHasProgramEntity(result))
            {
                InitUserCodFlow(result, context);
            }
            else {
                var messageActivity = context.Activity.AsMessageActivity();
                context.Done(messageActivity);
            }
        }

        private void InitUserCodFlow(LuisResult result, IDialogContext context)
        {
            var program = result.GetEntity("Program");
            Program = program.Entity;
            var form = FormDialog.FromForm(UserCodeFormFlow.BuildForm, FormOptions.PromptInStart);
            context.Call(form, FixLocalUserProblem);
        }

        private bool LuisResultHasProgramEntity(LuisResult result)
        {
            return result.GetEntity("Program") != null;
        }

        private async Task FixLocalUserProblem(IDialogContext context, IAwaitable<UserCodeFormFlow> result)
        {
            var userForm = await result;
            Request request = new Request
            {
                Category = "install_application",
                Date = DateTime.Now,
                Description = "Instalar aplicacion " + Program,
                UserCode = userForm.UserCode
            };

            request.Category = "";
            var NewRequest = await _externalService.MakeRequest(request);

            if (NewRequest != null)
            {
                await context.PostAsync("Hemos abierto una solicitud, el numero es: "+ NewRequest.RequestCode);
                context.Done(true);
            }
            context.Done(context.Activity);
        }
    }
}