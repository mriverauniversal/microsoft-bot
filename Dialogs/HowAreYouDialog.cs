﻿using LuisBot.Helpers;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System;
using System.Threading.Tasks;

namespace LuisBot.Dialogs
{
    [CustomLuisModel]
    [Serializable]
    public class HowAreYouDialog : LuisDialog<IMessageActivity>
    {
        [LuisIntent("HowAreYou")]
        public async Task HowAreYouIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Bien y tu?");
        }

        [LuisIntent("IAmFine")]
        public async Task IAmFine(IDialogContext context, LuisResult luisResult)
        {
            await context.PostAsync("Que bueno, me alegra mucho");
        }

        [LuisIntent("IAmBad")]
        public async Task IAmBad(IDialogContext context, LuisResult luisResult)
        {
            await context.PostAsync("Bueno, hay que estar positivo, las cosas pueden mejorar.");            
        }

        [LuisIntent("None")]
        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult luisResult)
        {
            var messageActivity = context.Activity.AsMessageActivity();
            context.Done(messageActivity);
        }
    }
}