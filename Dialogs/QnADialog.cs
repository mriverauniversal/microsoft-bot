﻿using Microsoft.Bot.Builder.Dialogs;
using QnAMakerDialog.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LuisBot.Dialogs
{
    [Serializable]
    public class QnADialog : QnAMakerDialog.QnAMakerDialog<object>
    {
        private double AcceptableScore { get; set; }
        public QnADialog()
        {
            SubscriptionKey = ConfigurationManager.AppSettings.Get("QnASuscriptionKey");
            KnowledgeBaseId = ConfigurationManager.AppSettings.Get("QnAKbId");
            MaxAnswers = int.Parse(ConfigurationManager.AppSettings.Get("QnAMaxAnswers"));
            AcceptableScore = double.Parse(ConfigurationManager.AppSettings.Get("QnAAcceptableScore"));
        }
        public override async Task StartAsync(IDialogContext context)
        {
            //Sobreescribimos este metodo porque no queremos que al inicializar este dialogo
            //El bot se quede esperando una respuesta del usuario, sino que obtenga el ultimo activity
            var message = context.Activity.AsMessageActivity();
            await this.MessageReceived(context, Awaitable.FromItem(message));
        }

        /// <summary>
        /// Este es el handler method por defecto si no se especifica un scrote handler
        /// </summary>
        public override async Task DefaultMatchHandler(IDialogContext context, string originalQueryText, QnAMakerResult result)
        {
            // ProcessResultAndCreateMessageActivity will remove any attachment markup from the results answer
            // and add any attachments to a new message activity with the message activity text set by default
            // to the answer property from the result
            var answer = result.Answers.FirstOrDefault(a => a.Score >= AcceptableScore);
            if (answer == null)
            {
                await NoMatchHandler(context, originalQueryText);
                return;
            }

            var messageActivity = ProcessResultAndCreateMessageActivity(context, ref result);
            messageActivity.Text = answer.Answer;
            await context.PostAsync(messageActivity);

            context.Done(messageActivity);
        }

        /// <summary>
        /// Handler used when the QnAMaker finds no appropriate answer
        /// </summary>
        public override async Task NoMatchHandler(IDialogContext context, string originalQueryText)
        {
            await context.PostAsync($"No le pude entender, favor dar mas detalles...");
            var message = context.Activity.AsMessageActivity();

            context.Done(message);
        }
    }
}