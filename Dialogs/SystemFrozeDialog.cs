﻿using LuisBot.FormFlow;
using LuisBot.Helpers;
using LuisBot.Interfaces;
using LuisBot.Services;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LuisBot.Dialogs
{
    [Serializable]
    [CustomLuisModel]
    public class SystemFrozeDialog : LuisDialog<IActivity>
    {
        private IExternalService _externalService;
        private string _systemName;
        public SystemFrozeDialog(string systemName)
        {
            _systemName = systemName;
            _externalService = new ExternalService();
        }

        [LuisIntent("LocalFroze")]
        public async Task LocalFroze(IDialogContext context, LuisResult luisResult)
        {
            var form = FormDialog.FromForm(UserCodeFormFlow.BuildForm, FormOptions.PromptInStart);
            context.Call(form, FixLocalUserProblem);
        }

        [LuisIntent("CitrixFroze")]
        public async Task CitrixFroze(IDialogContext context, LuisResult luisResult)
        {
            await context.PostAsync("Procedere a crear un solicitud para resolver su problema");
            var form = FormDialog.FromForm(UserCodeFormFlow.BuildForm, FormOptions.PromptInStart);
            context.Call(form, DoCitrixFixRequest);
        }

        [LuisIntent("None")]
        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult luisResult)
        {
            var messageActivity = context.Activity.AsMessageActivity();
            context.Done(messageActivity);
        }

        private async Task FixLocalUserProblem(IDialogContext context, IAwaitable<UserCodeFormFlow> result)
        {
            var userForm = await result;
            var isProblemFixed = await _externalService.KillUserSession(userForm.UserCode, _systemName);

            if (isProblemFixed)
            {
                await context.PostAsync("Solucionamos el problema, por favor intentelo de nuevo");
            }
            else
            {
                await context.PostAsync("Hemos tenido problemas para reestablecer su sesion, le notificaremos por correo cuando este solucionado.");
            }
            context.Done(context.Activity);
        }
        public override async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Su conexión es local o por citrix?");

            await base.StartAsync(context);

        }

        private async Task DoCitrixFixRequest(IDialogContext context, IAwaitable<UserCodeFormFlow> result)
        {
            var userForm = await result;
            var request = new ApiMockService.Models.Request
            {
                Category = "FIX",
                Date = DateTime.Now,
                Description = "Desbloqueo de Usuario de ACSEL en Citrix",
                UserCode = userForm.UserCode
            };

            request = await _externalService.MakeRequest(request);

            await context.PostAsync($"Se ha levantado una solicitud para resolver su problema, este el es codigo de la solicitud: {request.RequestCode}");
            context.Done(context.Activity);

        }

    }
}