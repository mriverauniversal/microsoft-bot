﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MockSupportService.Models
{
    public class Application
    {
        public string name { get; set; }
        public string status { get; set; }

        public Application()
        {
            name = "";
            status = "";
        }
    }
}