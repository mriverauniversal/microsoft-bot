﻿using ApiMockService.Models;
using LuisBot.Interfaces;
using MockSupportService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LuisBot.Services
{
    [Serializable]
    public class ExternalService : IExternalService
    {
        private string _baseUrl;
        public ExternalService()
        {
            _baseUrl = ConfigurationManager.AppSettings["BaseExternalServiceUri"];
        }
        public async Task<List<Application>> GetApplications()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.GetAsync($"{_baseUrl}/api/Application");
                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    var returnValue = JsonConvert.DeserializeObject<List<Application>>(jsonResponse);

                    return returnValue;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            

        }

        public async Task<bool> KillUserSession(string userCode, string systemName)
        {
            using (var httpClient = new HttpClient())
            {
                var obj = new
                {
                    userCode,
                    system = systemName
                };
                var jsonObj = JsonConvert.SerializeObject(obj);
                var content = new StringContent(jsonObj, Encoding.UTF8, "application/json");

                var response = await httpClient.PostAsync($"{_baseUrl}/api/Fix", content);

                var fixMade = response.StatusCode == System.Net.HttpStatusCode.OK;

                return fixMade;
            }
        }

        public async Task<Request> MakeRequest(Request request)
        {
            using (var httpClient = new HttpClient())
            {
                var jsonObj = JsonConvert.SerializeObject(request);
                var content = new StringContent(jsonObj, Encoding.UTF8, "application/json");

                var response = await httpClient.PostAsync($"{_baseUrl}/api/Request", content);

                var jsonResponse = await response.Content.ReadAsStringAsync();
                var returnValue = JsonConvert.DeserializeObject<Request>(jsonResponse);

                return returnValue;
            }
        }


        /*Request GetRequest(int RequestNumber) {

        }
        Request CreateRequest(Request request) {
        }

        Task killSession(string codusr) {
        }*/
    }
}