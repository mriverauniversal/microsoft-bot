﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LuisBot.FormFlow
{
    [Serializable]
    public class UserCodeFormFlow
    {
        [Prompt("Cual es tu codigo de usuario?")]
        public string UserCode { get; set; }

        public static IForm<UserCodeFormFlow> BuildForm()
        {
            return new FormBuilder<UserCodeFormFlow>()
                .Field("UserCode", validate: UserCodeValidation)
                .Build();
        }

        public static async Task<ValidateResult> UserCodeValidation(UserCodeFormFlow state, object value)
        {
            var userCode = (string)value;

            var result = new ValidateResult { IsValid = true, Value = value };
            var match = Regex.Match(userCode, @"U\d{5}$");
            if (match.Success)
            {
                state.UserCode = match.Value;
            } else {
                result.Feedback = "Disculpa pero ese no es un codigo de usuario valido.";
                result.IsValid = false;
            }

            return result;
        }
    }
}