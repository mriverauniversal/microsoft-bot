﻿using MockSupportService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuisBot.Interfaces
{
    public interface IExternalService
    {
        Task<List<Application>> GetApplications();
        Task<bool> KillUserSession(string userCode, string systemName);
        Task<ApiMockService.Models.Request> MakeRequest(ApiMockService.Models.Request request);
    }
}
