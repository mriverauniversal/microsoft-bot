﻿using System;
using Microsoft.Bot.Builder.FormFlow;
using LuisBot.FormFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace MicroBotTesting
{
    [TestClass]
    public class TestUserCodeFormFlow
    {
        UserCodeFormFlow formFlow;

        public TestUserCodeFormFlow()
        {
            formFlow = new UserCodeFormFlow();
        }

        [TestMethod]
        public void BuildForm_returnInstanceWithCorrectData()
        {
            var formFlow = UserCodeFormFlow.BuildForm();

            Assert.AreNotEqual(formFlow.Fields.Field("UserCode"), null );
        }

        [TestMethod]
        public void UserCodeValidation_returnIsValidIfTextHasCorrectUserCode()
        {
            Task<ValidateResult> validation = UserCodeFormFlow.UserCodeValidation(new UserCodeFormFlow(), "Mi codigo es U29586");

            Assert.AreEqual(validation.Result.IsValid, true);
        }

        [TestMethod]
        public void UserCodeValidation_returnIsNotValidIfTextNotHasCorrectUserCode()
        {
            Task<ValidateResult> validation = UserCodeFormFlow.UserCodeValidation(new UserCodeFormFlow(), "Mi codigo es UAPA");

            Assert.AreEqual(validation.Result.IsValid, false);
        }
    }
}
