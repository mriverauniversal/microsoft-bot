﻿using System;
using ApiMockService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MicroBotTesting
{
    [TestClass]
    public class TestRequest
    {
        Request request;

        public TestRequest()
        {
            request = new Request();
        }

        [TestMethod]
        public void TestUserCodeProperty_canBeSet()
        {
            request.UserCode = "U29586";

            Assert.AreEqual(request.UserCode, "U29586");
        }

        [TestMethod]
        public void TestUserCodeProperty_hasDefaultValue()
        {
            Assert.AreEqual(request.UserCode, "");
        }

        [TestMethod]
        public void TestDateProperty_canBeSet()
        {
            DateTime date = new DateTime();

            request.Date = date;

            Assert.AreEqual(request.Date, date);
        }

        [TestMethod]
        public void TestDateProperty_hasDefaultValue()
        {
            Assert.AreNotEqual(request.Date, null);
        }

        [TestMethod]
        public void TestDescriptionProperty_canBeSet()
        {
            request.Description = "My description";

            Assert.AreEqual(request.Description, "My description");
        }

        [TestMethod]
        public void TestDescriptionProperty_hasDefaultValue()
        {
            Assert.AreEqual(request.Description, "");
        }

        [TestMethod]
        public void TestCategoryProperty_canBeSet()
        {
            request.Category = "My category";

            Assert.AreEqual(request.Category, "My category");
        }

        [TestMethod]
        public void TestCategoryProperty_hasDefaultValue()
        {
            Assert.AreEqual(request.Category, "");
        }

        [TestMethod]
        public void TestRequestCodeProperty_canBeSet()
        {
            request.RequestCode = "R-12345";

            Assert.AreEqual(request.RequestCode, "R-12345");
        }

        [TestMethod]
        public void TestRequestCodeProperty_hasDefaultValue()
        {
            Assert.AreEqual(request.RequestCode, "");
        }
    }
}
