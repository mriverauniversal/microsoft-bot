﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockSupportService.Models;

namespace MicroBotTesting
{
    [TestClass]
    public class TestApplication
    {
        Application application;

        public TestApplication()
        {
            application = new Application();
        }

        [TestMethod]
        public void TestNameProperty_canBeSet()
        {
            application.name = "VMU";

            Assert.AreEqual(application.name, "VMU");
        }

        [TestMethod]
        public void TestNameProperty_hasDefaultValue()
        {
            Assert.AreEqual(application.name, "");
        }

        [TestMethod]
        public void TestStatusProperty_canBeSet()
        {
            application.status = "ACTIVE";

            Assert.AreEqual(application.status, "ACTIVE");
        }

        [TestMethod]
        public void TestStatusProperty_hasDefaultValue()
        {
            Assert.AreEqual(application.status, "");
        }
    }
}
